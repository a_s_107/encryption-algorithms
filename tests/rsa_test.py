import unittest

import rsa

class TestRSA(unittest.TestCase):
  def test_should_getTrue_when_paramPrimeNum(self):
    self.assertEqual(True, rsa.isPrime(17))

  def test_should_getTrue_when_paramNotPrimeNum(self):
    self.assertEqual(False, rsa.isPrime(20))

  def test_should_getPairKeys_when_paramsCorrect(self):
    publicKey, privateKey = rsa.generateKeys(397, 401)
    privateMod = 158400
    self.assertEqual(159197, publicKey[1])
    self.assertEqual(159197, privateKey[1])
    self.assertEqual(publicKey[0] * privateKey[0] % privateMod, 1)

  def test_should_throwExceptionValueError_when_paramsPandQequal(self):
    self.assertRaises(ValueError, rsa.generateKeys, 501, 501)

  def test_should_throwExceptionValueError_when_paramsNotPrimeNum(self):
    self.assertRaises(ValueError, rsa.generateKeys, 340, 397)

  def test_should_getEncryptedText_when_oneBlock(self):
    publicKey = (343, 159197)
    text = 'no'
    self.assertEqual('75992', rsa.encryptionWithRSA(text, publicKey))

  def test_should_getDecryptedText_when_oneBlock(self):
    privateKey = (12007, 159197)
    encryptedText = '75992'
    self.assertEqual('no', rsa.decryptionWithRSA(encryptedText, privateKey))

  def test_should_getEncryptedText_when_multipleBlock(self):
    publicKey = (5, 23393)
    text = 'ooflamzento'
    self.assertEqual('64559827162441830234209004', rsa.encryptionWithRSA(text, publicKey))

  def test_should_getDecryptedText_when_multipleBlock(self):
    privateKey = (13853, 23393)
    encryptedText = '64559827162441830234209004'
    self.assertEqual('ooflamzento', rsa.decryptionWithRSA(encryptedText, privateKey))

  def test_should_throwExceptionValueError_when_modInvDoesNotExist(self):
    privateKey = (-7339, 72203)
    encryptedText = '6418512906387284884214088'
    self.assertRaises(ValueError, rsa.decryptionWithRSA, encryptedText, privateKey)
