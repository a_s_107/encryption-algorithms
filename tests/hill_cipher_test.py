import unittest

import hill_cipher

class TestHillCipher(unittest.TestCase):
  def test_should_throwExceptionValueError_when_inParamNotSquareMatrix2by2(self):
    self.assertRaises(ValueError, hill_cipher.calcSecondOrderDet, [[1, 2]])

  def test_should_getSecondOrderDet_when_calc(self):
    matrix = [[1, 3], [-2, 5]]
    det = 11
    result = hill_cipher.calcSecondOrderDet(matrix)
    self.assertEqual(det, result)

  def test_should_throwExceptionValueError_when_inParamNotSquareMatrix3by3(self):
    self.assertRaises(ValueError, hill_cipher.calcThirdOrderDet, [[1, 2, 3], [4, 5, 6]])

  def test_should_getThirdOrderDet_when_calc(self):
    matrix = [[0, -1, 0], [1, 3, -2], [2, 5, -1]]
    det = 3
    result = hill_cipher.calcThirdOrderDet(matrix)
    self.assertEqual(det, result)

  def test_should_throwExceptionValueError_when_inParamNotSquareMatrixNbyN(self):
    self.assertRaises(ValueError, hill_cipher.calcDetOfOrderN, [[1, 2, 3], [4, 5, 6], [7, 8, 9], [10, 11, 12]])

  def test_should_getFourthOrderDet_when_calc(self):
    matrix = [[-1, -4, 0, -2], [0, 1, 5, 4], [3, 1, 1, 0], [-1, 0, 2, 2]]
    det = -12
    result = hill_cipher.calcDetOfOrderN(matrix)
    self.assertEqual(det, result)

  def test_should_getFifthOrderDet_when_calc(self):
    matrix = [[-1, -4, 0, 0, -2], [0, 1, 1, 5, 4], [3, 1, 7, 1, 0], [0, 0, 2, 0, -3], [-1, 0, 4, 2, 2]]
    det = 996
    result = hill_cipher.calcDetOfOrderN(matrix)
    self.assertEqual(det, result)

  def test_should_getDet_when_calc(self):
    matrix = [[14, 7, 10, 13],[8, 7, 6, 11],[11, 8, 18, 18],[3, 23, 21, 8]]
    determinant = -11874
    result = hill_cipher.calcDetOfMatrix(matrix)
    self.assertEqual(determinant, result)

  def test_should_getMatrixOfAlgebraicComplements_when_calc(self):
    matrix = [[6, 24, 1], [13, 16, 10], [20, 17, 15]]
    returnMatrix = [[70, 5, -99], [-343, 70, 378], [224, -47, -216]]
    result = hill_cipher.getMatrixOfAlgebraicComplements(matrix)
    self.assertListEqual(returnMatrix, result)

  def test_should_getEncryptedText_when_encryption(self):
    key = 'edccfbbbbfdcmqfd'
    text = 'code is ready'
    encryptedText = 'ZVVKFJBCBDSW'
    resultOfEncryptionFunction = hill_cipher.encryptionWithHillCipher(text, key)
    self.assertEqual(encryptedText, resultOfEncryptionFunction)

  def test_should_getDecryptedText_when_decryption(self):
    key = 'edccfbbbbfdcmqfd'
    encryptedText = 'ZVVKFJBCBDSW'
    text = 'codeisreadyz'
    resultOfDecryptionFunction = hill_cipher.decryptionWithHillCipher(encryptedText, key)
    self.assertEqual(text, resultOfDecryptionFunction)

  def test_should_throwExceptionValueError_when_encryptionKeyIsNotRepresentedBySquareMatrix(self):
    self.assertRaises(ValueError, hill_cipher.encryptionWithHillCipher, 'ask', 'adc')

  def test_should_throwExceptionValueError_when_decryptionKeyIsNotRepresentedBySquareMatrix(self):
    self.assertRaises(ValueError, hill_cipher.encryptionWithHillCipher, 'ask', 'adce')

  def test_should_throwExceptionValueError_when_encryptionMatrixComposedByKeyIsNotInvertible(self):
    self.assertRaises(ValueError, hill_cipher.encryptionWithHillCipher, 'as', 'adce')

  def test_should_throwExceptionValueError_when_decryptionMatrixComposedByKeyIsNotInvertible(self):
    self.assertRaises(ValueError, hill_cipher.encryptionWithHillCipher, 'as', 'adce')

  def test_should_leaveOnlyCharsFromAlphabetInText_when_encrypted(self):
    text = 'abCd,efgh.ijkl Mnop1qrst3uvwxyz'
    encryptedText = 'RHWOBDOULZGAVVYGFRQMPNISAYNQ'
    resultOfEncryptionFunction = hill_cipher.encryptionWithHillCipher(text, 'edccfbbbbfdcmqfd')
    self.assertEqual(encryptedText, resultOfEncryptionFunction)

  def test_should_leaveOnlyCharsFromAlphabetInText_when_decrypted(self):
    encryptedText = 'RHWOB DOULZGAVVY,GFRQMPNISA.YNQ'
    text = 'abcdefghijklmnopqrstuvwxyzzz'
    resultOfDecryptionFunction = hill_cipher.decryptionWithHillCipher(encryptedText, 'edccfbbbbfdcmqfd')
    self.assertEqual(text, resultOfDecryptionFunction)

  def test_should_leaveOnlyCharsFromAlphabetInKey_when_encrypted(self):
    text = 'abcdefghijklmnopqrstuvwxyz'
    encryptedText = 'RHWOBDOULZGAVVYGFRQMPNISAYNQ'
    resultOfEncryptionFunction = hill_cipher.encryptionWithHillCipher(text, 'edcc,fbbbb fdc.mqfd')
    self.assertEqual(encryptedText, resultOfEncryptionFunction)

  def test_should_leaveOnlyCharsFromAlphabetInKey_when_decrypted(self):
    encryptedText = 'RHWOBDOULZGAVVYGFRQMPNISAYNQ'
    text = 'abcdefghijklmnopqrstuvwxyzzz'
    resultOfDecryptionFunction = hill_cipher.decryptionWithHillCipher(encryptedText, 'ed,ccfbb.bbfdc mqfd')
    self.assertEqual(text, resultOfDecryptionFunction)

  def test_should_throwExceptionValueError_when_encryptingAndThirdParamIsNotString(self):
    self.assertRaises(ValueError, hill_cipher.encryptionWithHillCipher, 'abc', 'key', 6)

  def test_should_throwExceptionValueError_when_decryptingAndThirdParamIsNotString(self):
    self.assertRaises(ValueError, hill_cipher.decryptionWithHillCipher, 'ABC', 'key', None)

  def test_should_encryptUsingGivenAlphabet_when_thirdParamIsString(self):
    text = 'yes, this is it'
    encryptedText = 'GKDSHSVJJRASYELD'
    alphabet = 'abcdefghijklmnopqrstuvwxyz,. '
    resultOfEncryptionFunction = hill_cipher.encryptionWithHillCipher(text, 'ohknihgllissdxvi', alphabet)
    self.assertEqual(encryptedText, resultOfEncryptionFunction)

  def test_should_decryptUsingGivenAlphabet_when_thirdParamIsString(self):
    encryptedText = 'GKDSHSVJJRASYELD'
    text = 'yes, this is it '
    alphabet = 'abcdefghijklmnopqrstuvwxyz,. '
    resultOfDecryptionFunction = hill_cipher.decryptionWithHillCipher(encryptedText, 'ohknihgllissdxvi', alphabet)
    self.assertEqual(text, resultOfDecryptionFunction)
