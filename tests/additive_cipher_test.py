import unittest

import additive_cipher

class TestAdditiveCipher(unittest.TestCase):
  def test_should_ringShiftRight_when_encrypted(self):
    text = 'abcdefghijklmnopqrstuvwxyz'
    encryptedText = 'IJKLMNOPQRSTUVWXYZABCDEFGH'
    resultOfEncryptionFunction = additive_cipher.encryptionWithAdditiveCipher(text, 8)
    self.assertEqual(encryptedText, resultOfEncryptionFunction)

  def test_should_ringShiftLeft_when_decrypted(self):
    encryptedText = 'IJKLMNOPQRSTUVWXYZABCDEFGH'
    text = 'abcdefghijklmnopqrstuvwxyz'
    resultOfDecryptionFunction = additive_cipher.decryptionWithAdditiveCipher(encryptedText, 8)
    self.assertEqual(text, resultOfDecryptionFunction)

  def test_should_removeCharsMissingFromAlphabet_when_encrypted(self):
    text = 'abCd,efgh.ijkl Mnop1qrst3uvwxyz'
    encryptedText = 'PQRSTUVWXYZABCDEFGHIJKLMNO'
    resultOfEncryptionFunction = additive_cipher.encryptionWithAdditiveCipher(text, 15)
    self.assertEqual(encryptedText, resultOfEncryptionFunction)

  def test_should_removeCharsMissingFromAlphabet_when_decrypted(self):
    encryptedText = 'pQRS TUVWX,YZAB.cDEFG?HIJKLMNO'
    text = 'abcdefghijklmnopqrstuvwxyz'
    resultOfDecryptionFunction = additive_cipher.decryptionWithAdditiveCipher(encryptedText, 15)
    self.assertEqual(text, resultOfDecryptionFunction)

  def test_should_throwExceptionValueError_when_encryptingAndThirdParamIsNotString(self):
    self.assertRaises(ValueError, additive_cipher.encryptionWithAdditiveCipher, 'abc', 4, 6)

  def test_should_throwExceptionValueError_when_decryptingAndThirdParamIsNotString(self):
    self.assertRaises(ValueError, additive_cipher.decryptionWithAdditiveCipher, 'ABC', 6, None)

  def test_should_encryptUsingGivenAlphabet_when_thirdParamIsString(self):
    text = 'xyz'
    encryptedText = 'Z,.'
    alphabet = 'abcdefghijklmnopqrstuvwxyz,. '
    resultOfEncryptionFunction = additive_cipher.encryptionWithAdditiveCipher(text, 2, alphabet)
    self.assertEqual(encryptedText, resultOfEncryptionFunction)

  def test_should_decryptUsingGivenAlphabet_when_thirdParamIsString(self):
    encryptedText = 'CLZEG,OPZGPZGP,'
    text = 'yes, this is it'
    alphabet = 'abcdefghijklmnopqrstuvwxyz,. '
    resultOfDecryptionFunction = additive_cipher.decryptionWithAdditiveCipher(encryptedText, 7, alphabet)
    self.assertEqual(text, resultOfDecryptionFunction)
