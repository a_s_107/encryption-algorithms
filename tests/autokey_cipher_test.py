import unittest

import autokey_cipher

class TestAutokeyCipher(unittest.TestCase):
  def test_should_getEncryptedText_when_encryption(self):
    text = 'abcdefghijklmnopqrstuvwxyz'
    encryptedText = 'HBDFHJLNPRTVXZBDFHJLNPRTVX'
    resultOfEncryptionFunction = autokey_cipher.encryptionWithAutokeyCipher(text, 7)
    self.assertEqual(encryptedText, resultOfEncryptionFunction)

  def test_should_getDecryptedText_when_decryption(self):
    encryptedText = 'HBDFHJLNPRTVXZBDFHJLNPRTVX'
    text = 'abcdefghijklmnopqrstuvwxyz'
    resultOfDecryptionFunction = autokey_cipher.decryptionWithAutokeyCipher(encryptedText, 7)
    self.assertEqual(text, resultOfDecryptionFunction)

  def test_should_removeCharsMissingFromAlphabet_when_encrypted(self):
    text = 'abCd,efgh.ijkl Mnop1qrst3uvwxyz'
    encryptedText = 'MBDFHJLNPRTVXZBDFHJLNPRTVX'
    resultOfEncryptionFunction = autokey_cipher.encryptionWithAutokeyCipher(text, 12)
    self.assertEqual(encryptedText, resultOfEncryptionFunction)

  def test_should_removeCharsMissingFromAlphabet_when_decrypted(self):
    encryptedText = 'mBDFH JLNPRT,VXZB DFHJL/NPRTVX'
    text = 'abcdefghijklmnopqrstuvwxyz'
    resultOfDecryptionFunction = autokey_cipher.decryptionWithAutokeyCipher(encryptedText, 12)
    self.assertEqual(text, resultOfDecryptionFunction)

  def test_should_throwExceptionValueError_when_encryptingAndThirdParamIsNotString(self):
    self.assertRaises(ValueError, autokey_cipher.encryptionWithAutokeyCipher, 'abc', 8, 6)

  def test_should_throwExceptionValueError_when_decryptingAndThirdParamIsNotString(self):
    self.assertRaises(ValueError, autokey_cipher.decryptionWithAutokeyCipher, 'ABC', 1, None)

  def test_should_encryptUsingGivenAlphabet_when_thirdParamIsString(self):
    text = 'xyz'
    encryptedText = ' SU'
    alphabet = 'abcdefghijklmnopqrstuvwxyz,. '
    resultOfEncryptionFunction = autokey_cipher.encryptionWithAutokeyCipher(text, 5, alphabet)
    self.assertEqual(encryptedText, resultOfEncryptionFunction)

  def test_should_decryptUsingGivenAlphabet_when_thirdParamIsString(self):
    encryptedText = 'N WPZS,P,RH,RH.'
    text = 'yes, this is it'
    alphabet = 'abcdefghijklmnopqrstuvwxyz,. '
    resultOfDecryptionFunction = autokey_cipher.decryptionWithAutokeyCipher(encryptedText, 18, alphabet)
    self.assertEqual(text, resultOfDecryptionFunction)
