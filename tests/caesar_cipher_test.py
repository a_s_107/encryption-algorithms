import unittest

import caesar_cipher

class TestCipher(unittest.TestCase):
  def test_should_ringShiftRightBy3_when_encrypted(self):
    text = 'abcdefghijklmnopqrstuvwxyz'
    encryptedText = 'DEFGHIJKLMNOPQRSTUVWXYZABC'
    resultOfEncryptionFunction = caesar_cipher.encryptionWithCaesarCipher(text)
    self.assertEqual(encryptedText, resultOfEncryptionFunction)

  def test_should_ringShiftLeftBy3_when_decrypted(self):
    encryptedText = 'DEFGHIJKLMNOPQRSTUVWXYZABC'
    text = 'abcdefghijklmnopqrstuvwxyz'
    resultOfDecryptionFunction = caesar_cipher.decryptionWithCaesarCipher(encryptedText)
    self.assertEqual(text, resultOfDecryptionFunction)

  def test_should_encryptUsingGivenAlphabet_when_secondParamIsString(self):
    text = 'встретимся на нашем месте.'
    encryptedText = 'ЕФХУИХЛПФ ВРГВРГЫИПВПИФХИБ'
    alphabet = 'абвгдежзийклмнопрстуфхцчшщъыьэюя,. '
    resultOfEncryptionFunction = caesar_cipher.encryptionWithCaesarCipher(text, alphabet)
    self.assertEqual(encryptedText, resultOfEncryptionFunction)

  def test_should_decryptUsingGivenAlphabet_when_secondParamIsString(self):
    encryptedText = 'ЕФХУИХЛПФ ВРГВРГЫИПВПИФХИБ'
    text = 'встретимся на нашем месте.'
    alphabet = 'абвгдежзийклмнопрстуфхцчшщъыьэюя,. '
    resultOfDecryptionFunction = caesar_cipher.decryptionWithCaesarCipher(encryptedText, alphabet)
    self.assertEqual(text, resultOfDecryptionFunction)
