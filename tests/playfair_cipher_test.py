import unittest

import playfair_cipher

class TestPlayfairCipher(unittest.TestCase):
  def test_should_removeDuplicateChars_when_repeatedCharsInString(self):
    self.assertEqual('abc', playfair_cipher.removeDuplicateChars('aaaabbbccccbbbaaaaccc'))

  def test_should_rebuildAlphabet_when_thereIsPartOfAlphabet(self):
    self.assertEqual('febacd', playfair_cipher.getAlphabet('feb', 'abcdef'))

  def test_should_throwExceptionValueError_when_encryptingAndThirdParamIsNotString(self):
    self.assertRaises(TypeError, playfair_cipher.encryptionWithPlayfairCipher, 'abc', 'key', 6)

  def test_should_throwExceptionValueError_when_decryptingAndThirdParamIsNotString(self):
    self.assertRaises(TypeError, playfair_cipher.decryptionWithPlayfairCipher, 'ABC', 'key', None)

  def test_should_throwExceptionValueError_when_encryptingAndFourthParamIsNotString(self):
    self.assertRaises(TypeError, playfair_cipher.encryptionWithPlayfairCipher, 'text', 'key', 'alphabet', '')

  def test_should_throwExceptionValueError_when_decryptingAndFourthParamIsNotString(self):
    self.assertRaises(TypeError, playfair_cipher.decryptionWithPlayfairCipher, 'text', 'key', 'alphabet', 3.1)

  def test_should_throwExceptionValueError_when_lengthOfAlphabetIsNotDivByWidthOfMatrixForEncryption(self):
    self.assertRaises(ValueError, playfair_cipher.encryptionWithPlayfairCipher, 'text', 'key', 'abcdefghij', 4)

  def test_should_throwExceptionValueError_when_lengthOfAlphabetIsNotDivByWidthOfMatrixForDecryption(self):
    self.assertRaises(ValueError, playfair_cipher.decryptionWithPlayfairCipher, 'text', 'key', 'abcdefghij', 4)

  def test_should_getEncryptedText_when_encryption(self):
    key = 'lgdbaqmhecurnifxvsokzywtp'
    text = 'hello'
    encryptedText = 'ECQZBX'
    resultOfEncryptionFunction = playfair_cipher.encryptionWithPlayfairCipher(text, key)
    self.assertEqual(encryptedText, resultOfEncryptionFunction)

  def test_should_getDecryptedText_when_decryption(self):
    key = 'lgdbaqmhecurnifxvsokzywtp'
    encryptedText = 'ECQZBX'
    text = 'helxlo'
    resultOfDecryptionFunction = playfair_cipher.decryptionWithPlayfairCipher(encryptedText, key)
    self.assertEqual(text, resultOfDecryptionFunction)

  def test_should_getEncryptedAphabet_when_encryption(self):
    key = 'playfair'
    text = 'abcdefghijklmnopqrstuvwxyz'
    encryptedText = 'BHDIMPHKCUCEFGOQANCONZWXYCUZ'
    resultOfEncryptionFunction = playfair_cipher.encryptionWithPlayfairCipher(text, key)
    self.assertEqual(encryptedText, resultOfEncryptionFunction)

  def test_should_getDecryptedAphabet_when_decryption(self):
    key = 'playfair'
    encryptedText = 'BHDIMPHKCUCEFGOQANCONZWXYCUZ'
    text = 'abcdefghixiklmnopqrstuvwxyzx'
    resultOfDecryptionFunction = playfair_cipher.decryptionWithPlayfairCipher(encryptedText, key)
    self.assertEqual(text, resultOfDecryptionFunction)

  def test_should_getEncryptedCyrillicAphabet_when_encryption(self):
    key = 'русскийязык'
    text = alphabet = 'абвгдежзийклмнопрстуфхцчшщъыьэюя'
    encryptedText = 'БВЫЖЕЖОВЙЯРОНОЛХУКЧЙХПЧШЩЦРГЭЮЭЗ'
    resultOfEncryptionFunction = playfair_cipher.encryptionWithPlayfairCipher(text, key,  alphabet, 4)
    self.assertEqual(encryptedText, resultOfEncryptionFunction)

  def test_should_getDecryptedCyrillicAphabet_when_decryption(self):
    key = 'русскийязык'
    encryptedText = 'БВЫЖЕЖОВЙЯРОНОЛХУКЧЙХПЧШЩЦРГЭЮЭЗ'
    text = alphabet = 'абвгдежзийклмнопрстуфхцчшщъыьэюя'
    resultOfDecryptionFunction = playfair_cipher.decryptionWithPlayfairCipher(encryptedText, key, alphabet, 4)
    self.assertEqual(text, resultOfDecryptionFunction)
