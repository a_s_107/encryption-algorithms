import unittest

import multiplicative_cipher

class TestMultiplicativeCipher(unittest.TestCase):
  def test_should_getMultiplicativeInversion_when_paramsMutuallySimple(self):
    self.assertEqual(-5, multiplicative_cipher.getMultiplicativeInversion(5, 26))

  def test_should_throwExceptionValueError_when_paramsNotMutuallySimple(self):
    self.assertRaises(ValueError, multiplicative_cipher.getMultiplicativeInversion, 2, 26)

  def test_throwExceptionValueError_when_keyHasNoMultiplicativeInversionOnEncryption(self):
    self.assertRaises(ValueError, multiplicative_cipher.encryptionWithMultiplicativeCipher, 'abc', 2)

  def test_throwExceptionValueError_when_keyHasNoMultiplicativeInversionOnDecryption(self):
    self.assertRaises(ValueError, multiplicative_cipher.decryptionWithMultiplicativeCipher, 'abc', 13)

  def test_should_getTextWhereEachCharIsMultipliedByKey_when_encryption(self):
    text = 'abcdefghijklmnopqrstuvwxyz'
    encryptedText = 'ALWHSDOZKVGRCNYJUFQBMXITEP'
    resultOfEncryptionFunction = multiplicative_cipher.encryptionWithMultiplicativeCipher(text, 11)
    self.assertEqual(encryptedText, resultOfEncryptionFunction)

  def test_should_getTextWhereEachCharIsMultipliedByMultiplicativeInverseOfKey_when_decryption(self):
    encryptedText = 'ALWHSDOZKVGRCNYJUFQBMXITEP'
    text = 'abcdefghijklmnopqrstuvwxyz'
    resultOfDecryptionFunction = multiplicative_cipher.decryptionWithMultiplicativeCipher(encryptedText, 11)
    self.assertEqual(text, resultOfDecryptionFunction)

  def test_should_removeCharsMissingFromAlphabet_when_encrypted(self):
    text = 'abCd,efgh.ijkl Mnop1qrst3uvwxyz'
    encryptedText = 'AHOVCJQXELSZGNUBIPWDKRYFMT'
    resultOfEncryptionFunction = multiplicative_cipher.encryptionWithMultiplicativeCipher(text, 7)
    self.assertEqual(encryptedText, resultOfEncryptionFunction)

  def test_should_removeCharsMissingFromAlphabet_when_decrypted(self):
    encryptedText = 'aHOV CJQXE,LSZGN.UBIPWD!KRYFMT'
    text = 'abcdefghijklmnopqrstuvwxyz'
    resultOfDecryptionFunction = multiplicative_cipher.decryptionWithMultiplicativeCipher(encryptedText, 7)
    self.assertEqual(text, resultOfDecryptionFunction)

  def test_should_throwExceptionValueError_when_encryptingAndThirdParamIsNotString(self):
    self.assertRaises(ValueError, multiplicative_cipher.encryptionWithMultiplicativeCipher, 'abc', 5, 6)

  def test_should_throwExceptionValueError_when_decryptingAndThirdParamIsNotString(self):
    self.assertRaises(ValueError, multiplicative_cipher.decryptionWithMultiplicativeCipher, 'ABC', 9, None)

  def test_should_encryptUsingGivenAlphabet_when_thirdParamIsString(self):
    text = 'now'
    encryptedText = 'KNI'
    alphabet = 'abcdefghijklmnopqrstuvwxyz,. '
    resultOfEncryptionFunction = multiplicative_cipher.encryptionWithMultiplicativeCipher(text, 3, alphabet)
    self.assertEqual(encryptedText, resultOfEncryptionFunction)

  def test_should_decryptUsingGivenAlphabet_when_thirdParamIsString(self):
    encryptedText = 'CKQHMEDUQMUQMUE'
    text = 'yes, this is it'
    alphabet = 'abcdefghijklmnopqrstuvwxyz,. '
    resultOfDecryptionFunction = multiplicative_cipher.decryptionWithMultiplicativeCipher(encryptedText, 17, alphabet)
    self.assertEqual(text, resultOfDecryptionFunction)
