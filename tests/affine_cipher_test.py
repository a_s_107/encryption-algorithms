import unittest

import affine_cipher

class TestAffineCipher(unittest.TestCase):
  def test_throwExceptionValueError_when_firstKeyHasNoMultiplicativeInversionOnEncryption(self):
    self.assertRaises(ValueError, affine_cipher.encryptionWithAffineCipher, 'abc', 2, 1)

  def test_throwExceptionValueError_when_firstKeyHasNoMultiplicativeInversionOnDecryption(self):
    self.assertRaises(ValueError, affine_cipher.decryptionWithAffineCipher, 'abc', 13, 1)

  def test_should_getEncryptedText_when_encryption(self):
    text = 'abcdefghijklmnopqrstuvwxyz'
    encryptedText = 'EPALWHSDOZKVGRCNYJUFQBMXIT'
    resultOfEncryptionFunction = affine_cipher.encryptionWithAffineCipher(text, 11, 4)
    self.assertEqual(encryptedText, resultOfEncryptionFunction)

  def test_should_getDecryptedText_when_decryption(self):
    encryptedText = 'EPALWHSDOZKVGRCNYJUFQBMXIT'
    text = 'abcdefghijklmnopqrstuvwxyz'
    resultOfDecryptionFunction = affine_cipher.decryptionWithAffineCipher(encryptedText, 11, 4)
    self.assertEqual(text, resultOfDecryptionFunction)

  def test_should_getAdditiveCipher_when_firstParam1(self):
    secondKey = 8
    text = 'abcdefghijklmnopqrstuvwxyz'
    encryptedText = 'IJKLMNOPQRSTUVWXYZABCDEFGH'
    resultOfEncryptionFunction = affine_cipher.encryptionWithAffineCipher(text, 1, secondKey)
    resultOfDecryptionFunction = affine_cipher.decryptionWithAffineCipher(encryptedText, 1, secondKey)
    self.assertEqual(encryptedText, resultOfEncryptionFunction)
    self.assertEqual(text, resultOfDecryptionFunction)

  def test_should_getMultiplicativeCipher_when_secondParam0(self):
    firstKey = 7
    text = 'abcdefghijklmnopqrstuvwxyz'
    encryptedText = 'AHOVCJQXELSZGNUBIPWDKRYFMT'
    resultOfEncryptionFunction = affine_cipher.encryptionWithAffineCipher(text, firstKey, 0)
    resultOfDecryptionFunction = affine_cipher.decryptionWithAffineCipher(encryptedText, firstKey, 0)
    self.assertEqual(encryptedText, resultOfEncryptionFunction)
    self.assertEqual(text, resultOfDecryptionFunction)

  def test_should_removeCharsMissingFromAlphabet_when_encrypted(self):
    text = 'abCd,efgh.ijkl Mnop1qrst3uvwxyz'
    encryptedText = 'CJQXELSZGNUBIPWDKRYFMTAHOV'
    resultOfEncryptionFunction = affine_cipher.encryptionWithAffineCipher(text, 7, 2)
    self.assertEqual(encryptedText, resultOfEncryptionFunction)

  def test_should_removeCharsMissingFromAlphabet_when_decrypted(self):
    encryptedText = 'cJQX ELSZGN,UBIPW.DKRYFMTAHOV'
    text = 'abcdefghijklmnopqrstuvwxyz'
    resultOfDecryptionFunction = affine_cipher.decryptionWithAffineCipher(encryptedText, 7, 2)
    self.assertEqual(text, resultOfDecryptionFunction)

  def test_should_throwExceptionValueError_when_encryptingAndFourthParamIsNotString(self):
    self.assertRaises(ValueError, affine_cipher.encryptionWithAffineCipher, 'abc', 5, 2, 6)

  def test_should_throwExceptionValueError_when_decryptingAndFourthParamIsNotString(self):
    self.assertRaises(ValueError, affine_cipher.decryptionWithAffineCipher, 'ABC', 9, 2, None)

  def test_should_encryptUsingGivenAlphabet_when_fourthParamIsString(self):
    text = 'now'
    encryptedText = 'PSN'
    alphabet = 'abcdefghijklmnopqrstuvwxyz,. '
    resultOfEncryptionFunction = affine_cipher.encryptionWithAffineCipher(text, 3, 5, alphabet)
    self.assertEqual(encryptedText, resultOfEncryptionFunction)

  def test_should_decryptUsingGivenAlphabet_when_fourthParamIsString(self):
    encryptedText = 'WBH.DYXLHDLHDLY'
    text = 'yes, this is it'
    alphabet = 'abcdefghijklmnopqrstuvwxyz,. '
    resultOfDecryptionFunction = affine_cipher.decryptionWithAffineCipher(encryptedText, 17, 20, alphabet)
    self.assertEqual(text, resultOfDecryptionFunction)
