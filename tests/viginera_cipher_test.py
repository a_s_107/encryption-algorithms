import unittest

import viginera_cipher

class TestVigineraCipher(unittest.TestCase):
  def test_should_getEncryptedText_when_encryption(self):
    text = 'sheislistening'
    encryptedText = 'HHWKSWXSLGNTCG'
    resultOfEncryptionFunction = viginera_cipher.encryptionWithVigineraCipher(text, 'PASCAL')
    self.assertEqual(encryptedText, resultOfEncryptionFunction)

  def test_should_getDecryptedText_when_decryption(self):
    encryptedText = 'HHWKSWXSLGNTCG'
    text = 'sheislistening'
    resultOfDecryptionFunction = viginera_cipher.decryptionWithVigineraCipher(encryptedText, 'PASCAL')
    self.assertEqual(text, resultOfDecryptionFunction)

  def test_should_leaveOnlyCharsFromAlphabetInText_when_encrypted(self):
    text = 'abCd,efgh.ijkl Mnop1qrst3uvwxyz'
    encryptedText = 'LFORRQKTWWVPXRADDCWFIIHBJD'
    resultOfEncryptionFunction = viginera_cipher.encryptionWithVigineraCipher(text, 'LEMONLEMONLE')
    self.assertEqual(encryptedText, resultOfEncryptionFunction)

  def test_should_leaveOnlyCharsFromAlphabetInText_when_decrypted(self):
    encryptedText = 'LfORR QKTWWVPX.RADDCWFII,HBJD'
    text = 'abcdefghijklmnopqrstuvwxyz'
    resultOfDecryptionFunction = viginera_cipher.decryptionWithVigineraCipher(encryptedText, 'LEMONLEMONLE')
    self.assertEqual(text, resultOfDecryptionFunction)

  def test_should_leaveOnlyCharsFromAlphabetInKey_when_encrypted(self):
    text = 'abcdefghijklmnopqrstuvwxyz'
    encryptedText = 'LFORRQKTWWVPXRADDCWFIIHBJD'
    resultOfEncryptionFunction = viginera_cipher.encryptionWithVigineraCipher(text, 'LEMONLEMONLE')
    self.assertEqual(encryptedText, resultOfEncryptionFunction)

  def test_should_leaveOnlyCharsFromAlphabetInKey_when_decrypted(self):
    encryptedText = 'LFORRQKTWWVPXRADDCWFIIHBJD'
    text = 'abcdefghijklmnopqrstuvwxyz'
    resultOfDecryptionFunction = viginera_cipher.decryptionWithVigineraCipher(encryptedText, 'LEMONLEMONLE')
    self.assertEqual(text, resultOfDecryptionFunction)

  def test_should_throwExceptionValueError_when_encryptingAndThirdParamIsNotString(self):
    self.assertRaises(ValueError, viginera_cipher.encryptionWithVigineraCipher, 'abc', 'key', 6)

  def test_should_throwExceptionValueError_when_decryptingAndThirdParamIsNotString(self):
    self.assertRaises(ValueError, viginera_cipher.decryptionWithVigineraCipher, 'ABC', 'key', None)

  def test_should_encryptUsingGivenAlphabet_when_thirdParamIsString(self):
    text = 'yes, this is it'
    encryptedText = 'QMYFMXYA,FQCDZL'
    alphabet = 'abcdefghijklmnopqrstuvwxyz,. '
    resultOfEncryptionFunction = viginera_cipher.encryptionWithVigineraCipher(text, 'viginer', alphabet)
    self.assertEqual(encryptedText, resultOfEncryptionFunction)

  def test_should_decryptUsingGivenAlphabet_when_thirdParamIsString(self):
    encryptedText = 'QMYFMXYA,FQCDZL'
    text = 'yes, this is it'
    alphabet = 'abcdefghijklmnopqrstuvwxyz,. '
    resultOfDecryptionFunction = viginera_cipher.decryptionWithVigineraCipher(encryptedText, 'viginer', alphabet)
    self.assertEqual(text, resultOfDecryptionFunction)
