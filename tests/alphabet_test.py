import unittest

from alphabet import Alphabet

class TestAlphabet(unittest.TestCase):
  def setUp(self):
    self.alphabet = Alphabet()

  def test_should_getAlphabetLenEqualTo26_when_defaultAlphabetIsUsed(self):
    self.assertEqual(26, self.alphabet.getNumOfChars())

  def test_should_convertTextToLowercaseByRemovingNonAlphabeticalChars_when_willReceiveText(self):
    self.assertEqual('abcd', self.alphabet.transformInLowercase('Abc,d !'))

  def test_should_convertTextToUppercaseByRemovingNonAlphabeticalChars_when_willReceiveText(self):
    self.assertEqual('ABCDEF', self.alphabet.transformInUpperCase('Abc,d. EF !'))

  def test_should_getCharNumberInAlphabet_when_inParamLowercaseChar(self):
    self.assertEqual(2, self.alphabet.getCharNumber('c'))

  def test_should_getCharNumberInAlphabet_when_inParamUppercaseChar(self):
    self.assertEqual(25, self.alphabet.getCharNumber('z'))

  def test_should_throwExceptionValueError_when_inParamCharIsNotFromAlphabet(self):
    self.assertRaises(ValueError, self.alphabet.getCharNumber, ' ')

  def test_should_getCharInLowercase_when_inParamCharNumberInAlphabet(self):
    self.assertEqual('f', self.alphabet.getCharInLowercase(5))

  def test_should_getCharInLowercase_when_paramIsNegativeDigit(self):
    self.assertEqual('x', self.alphabet.getCharInLowercase(-3))

  def test_should_getCharInLowercase_when_paramIsDigitOutsideAlphabet(self):
    self.assertEqual('b', self.alphabet.getCharInLowercase(27))

  def test_should_getCharInUppercase_when_inParamCharNumberInAlphabet(self):
    self.assertEqual('J', self.alphabet.getCharInUppercase(9))

  def test_should_getCharInUppercase_when_paramIsNegativeDigit(self):
    self.assertEqual('T', self.alphabet.getCharInUppercase(-7))

  def test_should_getCharInUppercase_when_paramIsDigitOutsideAlphabet(self):
    self.assertEqual('M', self.alphabet.getCharInUppercase(38))

  def test_should_getNumMatrix_when_paramIsText(self):
    text = 'codeisready'
    size = 4
    matrix = [[2, 14, 3, 4],[8, 18, 17, 4],[0, 3, 24, 25]]
    self.assertListEqual(matrix, self.alphabet.convertTextToNumMatrix(text, size))

  def test_should_getLowercaseText_when_paramIsNumMatrix(self):
    matrix = [[14, 7, 10, 13],[8, 7, 6, 11],[11, 8, 18, 18],[3, 23, 21, 8]]
    text = 'ohknihgllissdxvi'
    self.assertEqual(text, self.alphabet.convertNumMatrixToLowercaseText(matrix))

  def test_should_getUppercaseText_when_paramIsNumMatrix(self):
    matrix = [[2, 14, 3, 4],[8, 18, 17, 4],[0, 3, 24, 25]]
    text = 'CODEISREADYZ'
    self.assertEqual(text, self.alphabet.convertNumMatrixToUppercaseText(matrix))
