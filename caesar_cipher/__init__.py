import additive_cipher

def encryptionWithCaesarCipher(sourceText, alphabet=''):
  return additive_cipher.encryptionWithAdditiveCipher(sourceText, 3, alphabet)

def decryptionWithCaesarCipher(sourceText, alphabet=''):
  return additive_cipher.decryptionWithAdditiveCipher(sourceText, 3, alphabet)
