import math

from alphabet import Alphabet
from multiplicative_cipher import getMultiplicativeInversion

def encryptionWithAffineCipher(sourceText, firstKey, secondKey, alphabet=''):
  if alphabet != '' and not isinstance(alphabet, str):
    raise ValueError('Alphabet must be a string')

  alph = Alphabet() if alphabet == '' else Alphabet(alphabet)
  text = alph.transformInLowercase(sourceText)

  if math.gcd(firstKey, alph.getNumOfChars()) != 1:
    raise ValueError('The first key does not fit. The key and the number of letters in the alphabet must be mutually simple')

  result = ''

  for char in text:
    result += alph.getCharInUppercase(alph.getCharNumber(char) * firstKey + secondKey)

  return result

def decryptionWithAffineCipher(sourceText, firstKey, secondKey, alphabet=''):
  if alphabet != '' and not isinstance(alphabet, str):
    raise ValueError('Alphabet must be a string')

  alph = Alphabet() if alphabet == '' else Alphabet(alphabet)
  text = alph.transformInUpperCase(sourceText)
  multiplicativeInversion = getMultiplicativeInversion(firstKey, alph.getNumOfChars())

  result = ''

  for char in text:
    result += alph.getCharInLowercase((alph.getCharNumber(char) + (alph.getNumOfChars() - secondKey)) * multiplicativeInversion)

  return result
