import math

from alphabet import Alphabet

def encryptionWithAutokeyCipher(sourceText, key, alphabet=''):
  if alphabet != '' and not isinstance(alphabet, str):
    raise ValueError('Alphabet must be a string')

  alph = Alphabet() if alphabet == '' else Alphabet(alphabet)
  text = alph.transformInLowercase(sourceText)
  result = ''
  currentKey = key

  for char in text:
    charNumber = alph.getCharNumber(char)
    result += alph.getCharInUppercase(charNumber + currentKey)
    currentKey = charNumber

  return result

def decryptionWithAutokeyCipher(sourceText, key, alphabet=''):
  if alphabet != '' and not isinstance(alphabet, str):
    raise ValueError('Alphabet must be a string')

  alph = Alphabet() if alphabet == '' else Alphabet(alphabet)
  text = alph.transformInUpperCase(sourceText)
  result = ''
  currentKey = key

  for char in text:
    charNumber = alph.getCharNumber(char) - currentKey
    result += alph.getCharInLowercase(charNumber)
    currentKey = charNumber % alph.getNumOfChars()

  return result
