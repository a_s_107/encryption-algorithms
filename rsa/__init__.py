import re
import math
from random import randint

from alphabet import Alphabet
from multiplicative_cipher import getMultiplicativeInversion

def isPrime(number):
  i = 2
  while i * i <= number and number % i != 0:
    i += 1
  return i * i > number

def generateKeys(p, q):
  if p == q:
    raise ValueError('p should not be equal to q')

  if not (isPrime(p) and isPrime(q)):
    raise ValueError('p and q must be prime numbers')

  mod = p * q
  privateMod = (p - 1) * (q - 1)
  publicKey = randint(2, privateMod - 1)

  while (math.gcd(publicKey, privateMod) != 1):
    publicKey = randint(2, privateMod - 1)

  privateKey = getMultiplicativeInversion(publicKey, privateMod)
  return ((publicKey, mod), (privateKey, mod))

def autoGenerateKeys(lowerLimit = pow(10, 154), upperLimit = pow(10, 155)):
  if lowerLimit > upperLimit:
    raise ValueError('Upper limit must be greater than the lower')

  p = randint(lowerLimit, upperLimit)
  q = randint(lowerLimit, upperLimit)

  while not isPrime(p):
    p = randint(lowerLimit, upperLimit)

  while not isPrime(q) or p == q:
    q = randint(lowerLimit, upperLimit)

  return generateKeys(p, q)

def encryptionWithRSA(sourceText, publicKey, alphabet=''):
  if alphabet != '' and not isinstance(alphabet, str):
    raise ValueError('Alphabet must be a string')

  alph = Alphabet() if alphabet == '' else Alphabet(alphabet)
  text = alph.transformInLowercase(sourceText)
  key, mod = publicKey

  textInNumbers = ''

  for char in text:
    textInNumbers += str(alph.getCharNumber(char) + 10)

  result = ''

  unit = 0
  i = 0
  while i < len(textInNumbers):
    num = int(textInNumbers[i])
    if unit * 10 + num > mod:
      if num == 0 or i == len(textInNumbers) - 1:
        num = unit % 10 * 10 + num
        unit = unit // 10
      encryptedUnit = pow(unit, key, mod)
      k = 10
      while encryptedUnit * k < mod:
        result += '0'
        k *= 10
      result += str(encryptedUnit)
      unit = num
    else:
      unit = unit * 10 + num
    i += 1
  else:
    result += str(pow(unit, key, mod))

  return result

def decryptionWithRSA(sourceText, privateKey, alphabet=''):
  if alphabet != '' and not isinstance(alphabet, str):
    raise ValueError('Alphabet must be a string')

  alph = Alphabet() if alphabet == '' else Alphabet(alphabet)
  reg = re.compile(r'\D+')
  text = reg.sub('', sourceText)
  key, mod = privateKey

  textInNumbers = ''

  unit = 0
  i = 0
  auxiliarySymInUnit = 0
  while i < len(text):
    num = int(text[i])
    if (unit * 10 + num) * 10**auxiliarySymInUnit > mod:
      textInNumbers += str(pow(unit, key, mod))
      auxiliarySymInUnit = 0
      unit = num
    else:
      unit = unit * 10 + num
    if unit == 0 and num == 0:
      auxiliarySymInUnit += 1
    i += 1
  else:
    textInNumbers += str(pow(unit, key, mod))

  result = ''

  i = 0
  while (i < len(textInNumbers) - 1):
    char = textInNumbers[i : i + 2]
    result += alph.getCharInLowercase(int(char) - 10)
    i += 2

  return result
