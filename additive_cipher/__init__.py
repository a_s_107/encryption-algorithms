from alphabet import Alphabet

def encryptionWithAdditiveCipher(sourceText, key, alphabet=''):
  if alphabet != '' and not isinstance(alphabet, str):
    raise ValueError('Alphabet must be a string')

  alph = Alphabet() if alphabet == '' else Alphabet(alphabet)
  text = alph.transformInLowercase(sourceText)
  result = ''

  for char in text:
    result += alph.getCharInUppercase(alph.getCharNumber(char) + key)

  return result

def decryptionWithAdditiveCipher(sourceText, key, alphabet=''):
  if alphabet != '' and not isinstance(alphabet, str):
    raise ValueError('Alphabet must be a string')

  alph = Alphabet() if alphabet == '' else Alphabet(alphabet)
  text = alph.transformInUpperCase(sourceText)
  result = ''

  for char in text:
    result += alph.getCharInLowercase(alph.getCharNumber(char) - key)

  return result
