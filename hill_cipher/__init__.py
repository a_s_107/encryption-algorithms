import math
import numpy as np

from alphabet import Alphabet
from multiplicative_cipher import getMultiplicativeInversion

def calcSecondOrderDet(matrix):
  if len(matrix) != 2 or len(matrix[0]) != 2:
    raise ValueError('Input matrix not appropriately sized')

  return matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0]

def calcThirdOrderDet(matrix):
  if len(matrix) != 3 or len(matrix[0]) != 3:
    raise ValueError('Input matrix not appropriately sized')

  return matrix[0][0] * matrix[1][1] * matrix[2][2] \
    + matrix[0][1] * matrix[1][2] * matrix[2][0] \
    + matrix[0][2] * matrix[1][0] * matrix[2][1] \
    - matrix[0][2] * matrix[1][1] * matrix[2][0] \
    - matrix[0][1] * matrix[1][0] * matrix[2][2] \
    - matrix[0][0] * matrix[1][2] * matrix[2][1]

def calcDetOfOrderN(matrix):
  if len(matrix) != len(matrix[0]):
    raise ValueError('Input matrix is not square')

  result = 0
  i = 0

  for j in range(len(matrix)):
    minor = np.delete(np.delete(matrix, i, 0), j, 1)
    result += matrix[i][j] * (-1)**(i + j) * (calcThirdOrderDet(minor) if len(minor) == 3 else calcDetOfOrderN(minor))

  return result

def calcDetOfMatrix(matrix):
  if len(matrix) == 2:
    return calcSecondOrderDet(matrix)
  elif len(matrix) == 3:
    return calcThirdOrderDet(matrix)
  else:
    return calcDetOfOrderN(matrix)

def getMatrixOfAlgebraicComplements(matrix):
  if len(matrix) != len(matrix[0]):
    raise ValueError('Input matrix is not square')

  result = []
  row = []

  for i in range(len(matrix)):
    for j in range(len(matrix)):
      row.append((-1)**(i + j) * calcDetOfMatrix(np.delete(np.delete(matrix, i, 0), j, 1)))
    result.append(row)
    row = []

  return result

def encryptionWithHillCipher(sourceText, sourceKey, alphabet=''):
  if alphabet != '' and not isinstance(alphabet, str):
    raise ValueError('Alphabet must be a string')

  alph = Alphabet() if alphabet == '' else Alphabet(alphabet)
  text = alph.transformInLowercase(sourceText)
  key = alph.transformInLowercase(sourceKey)
  sizeMatrix = int(math.sqrt(len(key)))

  if int(sizeMatrix**2) != len(key):
    raise ValueError('It is impossible to compose a square matrix by the input key')

  keyMatrix = np.array(alph.convertTextToNumMatrix(key, sizeMatrix))
  det = calcDetOfMatrix(keyMatrix) % alph.getNumOfChars()

  if det == 0 or math.gcd(det, alph.getNumOfChars()) != 1:
    raise ValueError('The matrix composed by the input key is not invertible')

  textMatrix = np.array(alph.convertTextToNumMatrix(text, sizeMatrix))
  encryptedMatrix = np.dot(textMatrix, keyMatrix)
  result = alph.convertNumMatrixToUppercaseText(encryptedMatrix)

  return result

def decryptionWithHillCipher(sourceText, sourceKey, alphabet=''):
  if alphabet != '' and not isinstance(alphabet, str):
    raise ValueError('Alphabet must be a string')

  alph = Alphabet() if alphabet == '' else Alphabet(alphabet)
  text = alph.transformInUpperCase(sourceText)
  key = alph.transformInLowercase(sourceKey)
  sizeMatrix = int(math.sqrt(len(key)))

  if int(sizeMatrix**2) != len(key):
    raise ValueError('It is impossible to compose a square matrix by the input key')

  keyMatrix = np.array(alph.convertTextToNumMatrix(key, sizeMatrix))
  det = calcDetOfMatrix(keyMatrix) % alph.getNumOfChars()
  if det == 0 or math.gcd(det, alph.getNumOfChars()) != 1:
    raise ValueError('The matrix composed by the input key is not invertible')

  invertMatrix = np.array(getMatrixOfAlgebraicComplements(keyMatrix.T)) % alph.getNumOfChars()
  textMatrix = np.array(alph.convertTextToNumMatrix(text, sizeMatrix))
  multiplicativeInversionDet = getMultiplicativeInversion(det, alph.getNumOfChars())
  encryptedMatrix = np.dot(textMatrix, invertMatrix * multiplicativeInversionDet % alph.getNumOfChars())
  result = alph.convertNumMatrixToLowercaseText(encryptedMatrix)

  return result
