from alphabet import Alphabet

def encryptionWithVigineraCipher(sourceText, sourceKey, alphabet=''):
  if alphabet != '' and not isinstance(alphabet, str):
    raise ValueError('Alphabet must be a string')

  alph = Alphabet() if alphabet == '' else Alphabet(alphabet)
  text = alph.transformInLowercase(sourceText)
  key = alph.transformInLowercase(sourceKey)
  result = ''
  i = 0

  for char in text:
    result += alph.getCharInUppercase(alph.getCharNumber(char) + alph.getCharNumber(key[i]))
    i = (i + 1) % len(key)

  return result

def decryptionWithVigineraCipher(sourceText, sourceKey, alphabet=''):
  if alphabet != '' and not isinstance(alphabet, str):
    raise ValueError('Alphabet must be a string')

  alph = Alphabet() if alphabet == '' else Alphabet(alphabet)
  text = alph.transformInUpperCase(sourceText)
  key = alph.transformInLowercase(sourceKey)
  result = ''
  i = 0

  for char in text:
    result += alph.getCharInLowercase(alph.getCharNumber(char) - alph.getCharNumber(key[i]))
    i = (i + 1) % len(key)

  return result
