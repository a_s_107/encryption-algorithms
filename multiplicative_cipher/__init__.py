import math

from alphabet import Alphabet

def getMultiplicativeInversion(number, mod):
  r1 = mod
  r2 = number
  t1 = 0
  t2 = 1

  while (r2 > 0):
    q = r1 // r2

    r = r1 - q * r2
    r1 = r2
    r2 = r

    t = t1 - q * t2
    t1 = t2
    t2 = t

  if r1 != 1:
    raise ValueError('No multiplicative inversion')

  return t1

def encryptionWithMultiplicativeCipher(sourceText, key, alphabet=''):
  if alphabet != '' and not isinstance(alphabet, str):
    raise ValueError('Alphabet must be a string')

  alph = Alphabet() if alphabet == '' else Alphabet(alphabet)
  text = alph.transformInLowercase(sourceText)

  if math.gcd(key, alph.getNumOfChars()) != 1:
    raise ValueError('The key does not fit. The key and the number of letters in the alphabet must be mutually simple')

  result = ''

  for char in text:
    result += alph.getCharInUppercase(alph.getCharNumber(char) * key)

  return result

def decryptionWithMultiplicativeCipher(sourceText, key, alphabet=''):
  if alphabet != '' and not isinstance(alphabet, str):
    raise ValueError('Alphabet must be a string')

  alph = Alphabet() if alphabet == '' else Alphabet(alphabet)
  text = alph.transformInUpperCase(sourceText)
  multiplicativeInversion = getMultiplicativeInversion(key, alph.getNumOfChars())

  result = ''

  for char in text:
    result += alph.getCharInLowercase(alph.getCharNumber(char) * multiplicativeInversion)

  return result
