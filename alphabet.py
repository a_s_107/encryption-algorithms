import re

class Alphabet:
  def __init__(self, alphabet='abcdefghijklmnopqrstuvwxyz', widthMatrix=0):
    self._alphabet = alphabet.lower()
    self._widthMatrix = widthMatrix

  def getNumOfChars(self):
    return len(self._alphabet)

  def transformInLowercase(self, text):
    reg = re.compile(f'[^{self._alphabet}]')
    return reg.sub('', text.lower())

  def transformInUpperCase(self, text):
    reg = re.compile(f'[^{self._alphabet.upper()}]')
    return reg.sub('', text.upper())

  def getCharNumber(self, char):
    return self._alphabet.index(char.lower())

  def getCharInLowercase(self, number):
    return self._alphabet[number % len(self._alphabet)]

  def getCharInUppercase(self, number):
    return self._alphabet[number % len(self._alphabet)].upper()

  def convertTextToNumMatrix(self, text, size):
    matrix = []
    row = []
    i = 0

    for char in text:
      row.append(self.getCharNumber(char))
      if i == size - 1:
        matrix.append(row)
        row = []

      i = (i + 1) % size

    if i != 0:
      while i < size:
        row.append(self.getNumOfChars() - 1)
        i += 1
      matrix.append(row)

    return matrix

  def convertNumMatrixToLowercaseText(self, matrix):
    result = ''

    for row in matrix:
      for num in row:
        result += self.getCharInLowercase(num)

    return result

  def convertNumMatrixToUppercaseText(self, matrix):
    result = ''

    for row in matrix:
      for num in row:
        result += self.getCharInUppercase(num)

    return result

  def getCharInLowercaseFromAlphabetMatrix(self, indices):
    if self._widthMatrix == 0:
      raise ValueError('Matrix width not set')

    return self._alphabet[indices[0] % (self.getNumOfChars() // self._widthMatrix) * self._widthMatrix + indices[1] % self._widthMatrix].lower()

  def getCharInUppercaseFromAlphabetMatrix(self, indices):
    if self._widthMatrix == 0:
      raise ValueError('Matrix width not set')

    return self._alphabet[indices[0] % (self.getNumOfChars() // self._widthMatrix) * self._widthMatrix + indices[1] % self._widthMatrix].upper()

  def getIndicesOfCharInAlphabetMatrix(self, char):
    if self._widthMatrix == 0:
      raise ValueError('Matrix width not set')

    index = self._alphabet.index(char.lower())
    return (index // self._widthMatrix, index % self._widthMatrix)
