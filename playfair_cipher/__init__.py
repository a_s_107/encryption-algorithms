import re

from alphabet import Alphabet

def removeDuplicateChars(string):
  result = ''

  for char in string:
    if result.find(char) == -1 and char != '':
      result += char

  return result

def getAlphabet(part, alphabet):
  result = removeDuplicateChars(part)
  reg = re.compile(f'[{part}]')
  result += reg.sub('', alphabet)

  if len(result) != len(alphabet):
    raise ValueError('The specified part of the alphabet contains characters not from the alphabet')

  return result

def getPairOfEncryptedChars(pairOfChars, alph):
  indicesFirstChar = alph.getIndicesOfCharInAlphabetMatrix(pairOfChars[0])
  indicesSecondChar = alph.getIndicesOfCharInAlphabetMatrix(pairOfChars[1])

  if indicesFirstChar[0] == indicesSecondChar[0]:
    i = indicesFirstChar[0]
    return (
      alph.getCharInUppercaseFromAlphabetMatrix((i, indicesFirstChar[1] + 1)),
      alph.getCharInUppercaseFromAlphabetMatrix((i, indicesSecondChar[1] + 1))
    )
  elif indicesFirstChar[1] == indicesSecondChar[1]:
    j = indicesFirstChar[1]
    return (
      alph.getCharInUppercaseFromAlphabetMatrix((indicesFirstChar[0] + 1, j)),
      alph.getCharInUppercaseFromAlphabetMatrix((indicesSecondChar[0] + 1, j))
    )
  else:
    return (
      alph.getCharInUppercaseFromAlphabetMatrix((indicesFirstChar[0], indicesSecondChar[1])),
      alph.getCharInUppercaseFromAlphabetMatrix((indicesSecondChar[0], indicesFirstChar[1]))
    )

def getPairOfChars(pairOfChars, alph):
  indicesFirstChar = alph.getIndicesOfCharInAlphabetMatrix(pairOfChars[0])
  indicesSecondChar = alph.getIndicesOfCharInAlphabetMatrix(pairOfChars[1])

  if indicesFirstChar[0] == indicesSecondChar[0]:
    i = indicesFirstChar[0]
    return (
      alph.getCharInLowercaseFromAlphabetMatrix((i, indicesFirstChar[1] - 1)),
      alph.getCharInLowercaseFromAlphabetMatrix((i, indicesSecondChar[1] - 1))
    )
  elif indicesFirstChar[1] == indicesSecondChar[1]:
    j = indicesFirstChar[1]
    return (
      alph.getCharInLowercaseFromAlphabetMatrix((indicesFirstChar[0] - 1, j)),
      alph.getCharInLowercaseFromAlphabetMatrix((indicesSecondChar[0] - 1, j))
    )
  else:
    return (
      alph.getCharInLowercaseFromAlphabetMatrix((indicesFirstChar[0], indicesSecondChar[1])),
      alph.getCharInLowercaseFromAlphabetMatrix((indicesSecondChar[0], indicesFirstChar[1]))
    )

def encryptionWithPlayfairCipher(sourceText, sourceKey, alphabet='abcdefghiklmnopqrstuvwxyz', widthMatrix=5, replacements = {'j': 'i'}, dummyChar='x'):
  if alphabet != '' and not isinstance(alphabet, str):
    raise TypeError('Alphabet must be a string')

  if not isinstance(widthMatrix, int):
    raise TypeError('Matrix width should be int')

  if len(alphabet) % widthMatrix != 0:
    raise ValueError('It is not possible to get the matrix of the alphabet')

  for k, value in replacements.items():
    key = sourceKey.replace(k.lower(), value.lower())
    key = key.replace(k.upper(), value.upper())
    text = sourceText.replace(k.lower(), value.lower())
    text = text.replace(k.upper(), value.upper())

  alph = Alphabet(getAlphabet(key, alphabet), widthMatrix)
  text = alph.transformInLowercase(text)
  result = ''
  i = 0

  while i < len(text):
    if i == len(text) - 1 or text[i] == text[i + 1]:
      couple = (text[i], dummyChar)
      i += 1
    else:
      couple = (text[i], text[i + 1])
      i += 2

    result += ''.join(getPairOfEncryptedChars(couple, alph))

  return result

def decryptionWithPlayfairCipher(sourceText, sourceKey, alphabet='abcdefghiklmnopqrstuvwxyz', widthMatrix=5, replacements = {'j': 'i'}):
  if alphabet != '' and not isinstance(alphabet, str):
    raise TypeError('Alphabet must be a string')

  if not isinstance(widthMatrix, int):
    raise TypeError('Matrix width should be int')

  if len(alphabet) % widthMatrix != 0:
    raise ValueError('It is not possible to get the matrix of the alphabet')

  for k, value in replacements.items():
    key = sourceKey.replace(k.lower(), value.lower())
    key = key.replace(k.upper(), value.upper())
    text = sourceText.replace(k.lower(), value.lower())
    text = text.replace(k.upper(), value.upper())

  alph = Alphabet(getAlphabet(key, alphabet), widthMatrix)
  text = alph.transformInUpperCase(text)
  result = ''
  i = 0

  while i < len(text):
    result += ''.join(getPairOfChars((text[i], text[i + 1]), alph))
    i += 2

  return result
